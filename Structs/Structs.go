package Structs

type Wallet2 struct {
	ID      int     `json:"id"`
	ADDRESS string  `json:"address"`
	MONEY   float32 `json:"money"`
}

type Transfer2 struct {
	ID     int     `json:"id"`
	FROM   string  `json:"from"`
	TO     string  `json:"to"`
	AMOUNT float32 `json:"amount"`
}
