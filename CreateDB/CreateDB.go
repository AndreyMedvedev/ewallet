package CreateDB

import (
	"crypto/rand"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"math/big"
)

func CheckErr(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func cryptoAdd() string {
	b := make([]byte, 8)
	_, _ = rand.Read(b)

	n := new(big.Int)
	n.SetBytes(b)
	return n.Text(62)[:10]
}

func CreateTables(sqliteDB *sql.DB) {
	statement, err := sqliteDB.Prepare("CREATE TABLE IF NOT EXISTS wallet (wallet_id INTEGER PRIMARY KEY ASC ON CONFLICT ROLLBACK AUTOINCREMENT NOT NULL ON CONFLICT ROLLBACK, address TEXT, balance REAL CHECK (balance >= 0))")
	CheckErr(err)
	statement.Exec()

	statement, err = sqliteDB.Prepare("CREATE TABLE IF NOT EXISTS transfer (transfer_id INTEGER PRIMARY KEY ON CONFLICT ROLLBACK AUTOINCREMENT NOT NULL, wallet1_id TEXT REFERENCES wallet (address) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL, wallet2_id TEXT REFERENCES wallet (address) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL, summ REAL NOT NULL CHECK (summ > 0))")
	CheckErr(err)
	statement.Exec()

}

func InsertWallets(sqliteDB *sql.DB) {
	statement, err := sqliteDB.Prepare("INSERT INTO wallet (address, balance) VALUES (?, ?)")
	CheckErr(err)

	for i := 0; i < 10; i++ {
		_, err = statement.Exec(cryptoAdd(), 100.00)
		CheckErr(err)
	}
}

func FirstTry(sqliteDB *sql.DB) bool {
	row, err := sqliteDB.Query("SELECT * FROM wallet")
	CheckErr(err)
	defer row.Close()

	if !row.Next() {
		return true
	}
	return false
}
