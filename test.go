package main

import (
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	"go_test2/CreateDB"
	"go_test2/Structs"
	"log"
	"net/http"
	"strconv"
)

var sqliteDB *sql.DB

func GetBalance(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)

	walletAddress := params["address"]

	var wallet Structs.Wallet2

	err := sqliteDB.QueryRow("SELECT * FROM wallet WHERE address = ?", walletAddress).Scan(&wallet.ID, &wallet.ADDRESS, &wallet.MONEY)
	if err != nil {
		json.NewEncoder(w).Encode("Такого кошелька не существует")
		return
	}

	json.NewEncoder(w).Encode(wallet)
}

func GetLast(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	query := r.URL.Query()

	count := query.Get("count")

	count2, _ := strconv.Atoi(count)

	rows, err := sqliteDB.Query("SELECT * FROM transfer ORDER BY transfer_id DESC LIMIT ?", count2)
	defer rows.Close()

	CreateDB.CheckErr(err)

	var transfers []Structs.Transfer2

	for rows.Next() {
		var id int
		var from string
		var to string
		var amount float32

		err = rows.Scan(&id, &from, &to, &amount)

		CreateDB.CheckErr(err)

		transfers = append(transfers, Structs.Transfer2{ID: id, FROM: from, TO: to, AMOUNT: amount})
	}

	json.NewEncoder(w).Encode(transfers)

}

func Send(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var transfer Structs.Transfer2

	err := json.NewDecoder(r.Body).Decode(&transfer)
	if err != nil {
		json.NewEncoder(w).Encode("Введена некорректная сумма")
		return
	}

	if transfer.AMOUNT < 0 {
		json.NewEncoder(w).Encode("Введена отрицательная сумма перевода")
		return
	}

	if transfer.FROM == transfer.TO {
		json.NewEncoder(w).Encode("Некорректный запрос")
		return
	}

	var exists int

	err = sqliteDB.QueryRow("SELECT EXISTS (SELECT * FROM wallet WHERE address = ?) + EXISTS (SELECT * FROM wallet WHERE address = ?)", transfer.TO, transfer.FROM).Scan(&exists)

	if exists != 2 {
		json.NewEncoder(w).Encode("Некорректный запрос")
		return
	}

	statement1, err1 := sqliteDB.Prepare("INSERT INTO transfer (wallet1_id, wallet2_id, summ) VALUES (?,?,?)")
	CreateDB.CheckErr(err1)
	statement2, err2 := sqliteDB.Prepare("UPDATE wallet SET balance = balance - ? WHERE address = ?")
	CreateDB.CheckErr(err2)
	statement3, err3 := sqliteDB.Prepare("UPDATE wallet SET balance = balance + ? WHERE address = ?")
	CreateDB.CheckErr(err3)

	_, err2 = statement2.Exec(transfer.AMOUNT, transfer.FROM)
	if err2 != nil {
		json.NewEncoder(w).Encode("На кошельке недостаточно средств")
		return
	}

	_, err1 = statement1.Exec(transfer.FROM, transfer.TO, transfer.AMOUNT)
	if err1 != nil {
		json.NewEncoder(w).Encode("Некорректный запрос")
		return
	}

	_, err3 = statement3.Exec(transfer.AMOUNT, transfer.TO)
	if err3 != nil {
		json.NewEncoder(w).Encode("Некорректный запрос")
		return
	}

	json.NewEncoder(w).Encode("Перевод осуществлен")
}

func main() {
	database, err := sql.Open("sqlite3", "./test.db")
	CreateDB.CheckErr(err)
	sqliteDB = database
	defer database.Close()
	CreateDB.CreateTables(sqliteDB)

	if CreateDB.FirstTry(sqliteDB) {
		CreateDB.InsertWallets(sqliteDB)
	}

	router := mux.NewRouter()

	//GetBalance
	router.HandleFunc("/api/wallet/{address}/balance", GetBalance).Methods("GET")

	//GetLast
	router.HandleFunc("/api/transactions", GetLast).Methods("GET")

	//Send
	router.HandleFunc("/api/send", Send).Methods("POST")

	//serve the app
	log.Fatal(http.ListenAndServe(":8000", router))
}
